<?php

require_once __DIR__ . '/vendor/autoload.php';

// pre populate the dummy session data
class CreateDummyData{
	private $existingDevices;
	private $existingUsers;

	public function __construct(){
		$this->existingDevices = array();
		$this->existingUsers = array();
	}

	private function getUserAccess($session){

		$access = (string)new MongoId();
		$access .= ",".$session->appId;
		$access .= ",".$session->uxcamUserId;
		$access .= ",".$session->userId;
		$access .= ",".$session->deviceId;
		$access .= ",".$session->_id;
		$access .= ",".date("Y-m-d h:i:s")."+".rand(0,5);
		$access .= "\n";
		return $access;

		// $access = new stdClass();
		// // $access->_id = new MongoId();
		// $access->accessid = new MongoId();
		// $access->appId = $session->appId;
		// $access->uxcamUserId = $session->uxcamUserId;
		// $access->userId = $session->userId;
		// $access->deviceId = $session->deviceId;
		// $access->sessionId = $session->_id;
		// $access->lastSeenOn = date("Y-m-d h:i:s")."+".rand(0,5);
		// return $access;
	}

	private function getEvent($session){
		$eventName = ['Registered','Purchased','Sold','Page Visited'];
		$key = rand(0,3);
		$pkey = rand(0,1);
		$bool = [false, true];
		$event = new stdClass();
		$event->_id = new MongoId();
		$event->appId = $session->appId;
		$event->eventName = $eventName[$key];
		$event->uxcamUserId = $session->uxcamUserId;
		$event->deviceId = $session->deviceId;
		$event->userId = $session->userId;
		$event->sessionId = $session->_id;
		$event->trackedon = date("Y-m-d h:i:s")."+".rand(0,5);
		for($i=1; $i<=50; ++$i){
			$field = 'cp'.$i;
			$event->$field = 'cp'.$i;
		}
		return $event;
	}

	private function getDevice($session){
		if(in_array($session->deviceId.$session->appId, $this->existingDevices)){
			return null;
		}
		$device = new stdClass();
		$device->deviceId = $session->deviceId;
		$device->appId = $session->appId;
		$device->type = $session->deviceType;
		$device->platform = $session->platform;
		$device->model = $session->deviceModel;
		$device->marketingName = $session->deviceMarketingName;
		$device->manufacturer = $session->deviceManufacturer;
		$device->deviceClass = $session->deviceClass;
		$device->width = $session->deviceWidth;
		$device->height = $session->deviceHeight;
		$device->dpi = $session->deviceDpi;
		$device->isRooted = $session->deviceIsRooted;
		$device->totalRam = $session->deviceTotalRam;
		$device->storage = $session->deviceStorage;
		$device->country = $session->deviceCountry;
		$device->countryCode = $session->countryCode;
		$device->language = $session->deviceLanguage;
		$device->osName = $session->deviceOsName;
		$device->osVersion = $session->deviceOsVersion;
		$device->carriername = $session->deviceCarrierName;
		$device->carrierCode = $session->deviceCarrierCode;
		$device->createdOn = date("Y-m-d h:i:s")."+".rand(0,5);
		$device->lastUpdatedOn = date("Y-m-d h:i:s")."+".rand(0,5);
		$this->existingDevices[] = $session->deviceId.$session->appId;
		return $device;
	}

	private function getUser($session){
		if(in_array($session->uxcamUserId, $this->existingUsers)){
			return null;
		}
		$user = new stdClass();
		$user->uxcamUserId = $session->uxcamUserId;
		$user->appId = $session->appId;
		$user->userId = $session->userId;
		$user->city = $session->city;
		$user->country = $session->country;
		$user->countryCode = $session->countryCode;
		$user->userGroup = '';
		for($i=1; $i<=50; ++$i){
			$field = 'cp'.$i;
			$user->$field = '';
		}
		$user->registeredOn = date("Y-m-d h:i:s")."+".rand(0,5);
		$user->lastSeenOn = date("Y-m-d h:i:s")."+".rand(0,5);
		$this->existingUsers[] = $session->uxcamUserId;
		return $user;
	}

	private function getVideo($session){
		$video = new stdClass();
		$video->_id = $session->_id;
		$video->appId = $session->appId;
		$video->videoBucket = 'data.uxcam.com';
		$video->videoRegion = 'us-east-a';
		$video->videoKey = $session->orgId."/".$session->appId."/".$session->_id;
		$video->vieoFileName = 'video.mp4';
		$video->recordedOn = date("Y-m-d h:i:s")."+".rand(0,5);
		$video->uploadedOn = date("Y-m-d h:i:s")."+".rand(0,5);
		$video->status = 1;
		$video->delStatus = 0;
		$video->createdOn = date("Y-m-d h:i:s")."+".rand(0,5);
		return $video;
	}

	private function getSession(){
		$platform = ['android','ios'];
		$appId = ['591980359d1d13943000002a','591980359d1d139430000040','591980359d1d13943000006a','591980359d1d13943000009c'];
		$osName = ['android', 'ios'];
		$osVersion = ['5', '4', '3', '6'];
		$height = [780, 1080, 1200, 1980];
		$width = [340, 780, 1080, 1200];
		$dpi = [180, 200, 220, 240];
		$country = ['Nepal', 'USA', 'UK', 'India'];
		$countryCode = ['NP', 'US', 'UK', 'IN'];
		$language = ['EN-NP', 'EN-US', 'EN-UK', 'EN-IN'];
		$key = rand(0,3);
		$pkey = rand(0,1);
		$bool = [0,1];
		$session = new stdClass();
		$session->_id = new MongoId();
		$session->appId = $appId[$key];
		$session->orgId = 'O3092092091093892';
		$session->deviceType = 'Device Type'.rand(100,200);
		$session->platform = $platform[$pkey];
		$session->appVersion = '1.0.0';
		$session->sdkVersion = '1.0.0';
		$session->uxcamUserId = 'UxcamUser'.rand(100,200);
		$session->userId = '';
		$session->deviceId = 'DeviceId'.rand(100,200);
		$session->deviceModel = 'Device Model'.rand(100,400);
		$session->deviceMarketingName = 'SamsungI'.rand(90, 100);
		$session->deviceManufacturer = 'Samsung';
		$session->deviceClass= 'Android';
		$session->deviceWidth = $width[$key];
		$session->deviceHeight = $height[$key];
		$session->deviceDpi = $dpi[$key];
		$session->deviceIsRooted = $bool[$pkey];
		$session->deviceIsNotificationEnabled = $bool[$pkey];
		$session->deviceTotalRam = rand(1024, 4096);
		$session->deviceFreeRam = rand(400, 800);
		$session->deviceStorage = rand(1, 8);
		$session->deviceCountry = $country[$key];
		$session->deviceLanguage = $language[$key];
		$session->deviceOsName = $osName[$pkey];
		$session->deviceOsVersion = $osVersion[$key];
		$session->deviceCarrierName = 'Test Carrier '.rand(10, 20);
		$session->deviceCarrierCode = 'Test Carrier Code '.rand(10, 20);
		$session->totalSessionTime = rand(100, 500);
		$session->isFavourite = $bool[$pkey];
		$session->isCrashed = $bool[$pkey];
		$session->isOffline = $bool[$pkey];
		$session->recordedOn = date("Y-m-d h:i:s")."+".rand(0,5);
		$session->uploadStartedOn = date("Y-m-d h:i:s")."+".rand(0,5);
		$session->uploadedOn = date("Y-m-d h:i:s")."+".rand(0,5);
		$session->country = $country[$key];
		$session->countryCode = $countryCode[$key];
		$session->city = 'City name '.rand(100, 120);
		$session->latitude = 0;
		$session->longitude = 0;
		$session->deviceRun = 0;
		$session->deviceRunName = '';
		$session->sessionRun = 0;
		$session->sessionRunName = '';
		$session->isPlayed = $bool[$pkey];
		$session->apiVersion = '1.0.0';
		for($i=1; $i<=50; ++$i){
			$field = 'cp'.$i;
			$session->$field = '';
		}
		$session->createdOn = date("Y-m-d h:i:s")."+".rand(0,5);
		return $session;
	}

	private function getObjectText($object){
		$det = get_object_vars($object);
		$txt = implode(",", $det);
		return $txt;
	}

	public function createDataFile(){
		$sessionFile = fopen("session.csv", "a");
		$deviceFile = fopen("device.csv", "a");
		$userFile = fopen("app_user.csv","a");
		$eventFile = fopen("event.csv", "a");
		$useraccessFile = fopen("user_access.csv","a");
		$videoFile = fopen("video.csv","a");
		for($i=0; $i<5000; ++$i){
			$session = $this->getSession();
			$video = $this->getVideo($session);
			$eventCount = rand(10,20);
			for($j=0; $j< $eventCount; ++$j){
				$event = $this->getEvent($session);
				fwrite($eventFile, $this->getObjectText($event)."\n");
			}
			$device = null;
			$user = null;
			$device = $this->getDevice($session);
			if(!is_null($device)){
				fwrite($deviceFile, $this->getObjectText($device)."\n");
			}
			$user = $this->getUser($session);
			if(!is_null($user)){
				fwrite($userFile, $this->getObjectText($user)."\n");
			}
			$access = $this->getUserAccess($session);
			fwrite($useraccessFile, $access);
			fwrite($sessionFile, $this->getObjectText($session)."\n");
			fwrite($videoFile, $this->getObjectText($video)."\n");

		}
	}
}
$data = new CreateDummyData();
$data->createDataFile();

